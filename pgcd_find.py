"""Permet de determiner le PGCD de deux nombres entrée par l'utilisateur"""

# import des modules
from my_modules.pgcd_module import *
import argparse
import logging

parser = argparse.ArgumentParser(
    prog="pgcd_find.py",
    usage='%(prog)s [-options]',
    description=__doc__
)

# arguments de ligne de commande pour execution du script
parser.add_argument('a', type=int, help='premier nombre')
parser.add_argument('b', type=int, help='deuxieme nombre')
parser.add_argument('-v', '--verbose', action='store_true', help='Afficher\
la réponse complète')
parser.add_argument('-d', '--debug', action='store_true', help='affiche \
les evenements en console')
parser.add_argument('-l', '--logfile', action='store_true', help='Ecrit \
les evenements dans un fichier .log créé dans le dossier courant')

args = parser.parse_args()

# Assignation des arguments a des variables
a = args.a
b = args.b
verbose = args.verbose
debug = args.debug
logfile = args.logfile

# si option -d specifiee affiche evenements dans console
if debug:
    conf_logging(1)

# si option -l specifiee ecrit evenements dans un fichier de log
if logfile:
    conf_logging(2)

# verifie argument > 0
if a > 0 and b > 0:
    logging.info("appel de la fonction pgcd")
    resultatPgcd = pgcd(a, b)
    # si a < b retourne un message d'erreur
    if resultatPgcd == None:
        print("Erreur !")
    else:
        print(f"Le plus grand diviseur commun est {resultatPgcd}")
# argument =< 0 renvoi un message
else:
    print("'a' et 'b' doivent etre superieur a 0")

# si option -v specifiee affiche reponse complete
if verbose:
    print(f"PGCD({a},{b})={resultatPgcd}")