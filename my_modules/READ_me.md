Le dossier "my_modules" comprend les modules necessaire à l'utilisation du script
"pgcd_find.py"

Lors du clonage du projet, assurez vous que l'import du module est fait correctement dans le fichier "pgcd_find.py"

Utilisation : 

Executer le script avec la commande suivante : 
python.exe pgcd_find.py <1er nombre> <2eme nombre> <-option>

Pour afficher aide de pgcd_find.py:
python.exe pgcd_find.py -h

Le fichier "test_module.txt" : 

ce fichier sert a tester à l'aide du module doctest les fonctions du fichier "pgcd_module.py"
Si vous souhaitez utiliser le module doctest pour tester les fonctions du fichier "pgcd_module.py" sans avoir à faire une modification, il vous suffit d'executer "pgcd_module.py", le fichier est configuré pour que doctest lance les tests automatiquement. 

Si vous souhaitez l'utiliser, veuillez aller à la ligne 48 du fichier "pgcd_module.py" puis supprimer le caractères "#" , la ligne devrait ressembler à ça après modification :

doctest.testfile("test_module.txt", verbose=True)