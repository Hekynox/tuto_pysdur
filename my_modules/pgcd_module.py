"""
Le module 'pgcd_module' contient une ou des fonctions servant à calculer le 
plus grand diviseur (PGCD) commun a partir de deux nombres
"""

import logging

def pgcd(a, b):
    """
    Algorithme determinant le plus grand diviseur commun
    par méthode de soustractions successives. 

    'a' -> premier nombre
    'b' -> deuxieme nombre
    
    verifie que a > b sinon  a = b et b = a
    return -> PGCD 

    Exemple :
    >>> pgcd(561,357)
    51
    >>> pgcd(452,352)
    4
    >>> pgcd(564,322)
    2
    """
    if a < b:
        logging.critical('%s ne peut pas etre plus petit que %s', a, b)
        return
    else:
        while b != 0:
            logging.debug('%s - %s = %s', a, b, a - b)
            a, b = b, a % b
    return a

def conf_logging(conf_log):
    """
    Determine la configuration du module logging

    si conf_log = 1:
    Affiche evenement delogging en console

    si conf_log = 2:
    Ecrit evenement de logging dans un fichier 'debug.log'

    Exemple:
    Activer evenements en console:
    >>> conf_logging(1)

    Activer evenements ecrit en fichier:
    >>> conf_logging(2)
    """
    if conf_log == 1:
        logging.basicConfig(
            encoding='utf-8',
            format='%(name)s %(levelname)s %(asctime)s %(message)s',
            level=logging.DEBUG
        )
    elif conf_log == 2:
        logging.basicConfig(
            filename='debug.log',
            encoding='utf-8',
            format='%(name)s %(levelname)s %(asctime)s %(message)s',
            level=logging.DEBUG
        )
    else:
        return

if __name__ == "__main__":
     import doctest
#    doctest.testfile("test_module.txt", verbose=True)
     doctest.testmod(verbose=True)